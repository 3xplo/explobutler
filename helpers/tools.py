import datetime
import re


def time_now():
    return f"{datetime.datetime.now()}"


def check_bear_string(input_string):
    pattern = r'(?:(?:^|\s+)мишк|(?:^|\s+)медв|\U0001F43B)'
    regex_pattern = re.compile(pattern)

    return bool(regex_pattern.search(input_string.lower()))


SWEARS = ['сук', "бля", "хуй", "пох", "нах", "пиздец", "пизда", "ебать", "ебет", "ебёт"]


def is_swearing(text):
    pattern = r'\b(?:' + '|'.join(map(re.escape, SWEARS)) + r')'
    return bool(re.search(pattern, text.lower()))
