from telegram import Update, Message, ReactionType, MessageReactionUpdated

import constants


def gtext(message: Message):
    return message.text if message.text else message.caption


def is_sent_by_me(update: Update) -> bool:
    return is_message_by_me(update) or is_reaction_by_me(update)


def is_message_by_me(update: Update) -> bool:
    try:
        uid = update.message.from_user.id
        return uid == constants.EXPLO_ID or uid == constants.EXPLO_GROUP_ID
    except Exception:
        return False


def is_reaction_by_me(update: Update) -> bool:
    try:
        reaction = update.message_reaction
        return (reaction.user and reaction.user.id == constants.EXPLO_ID) or (
                reaction.actor_chat and reaction.actor_chat.id == constants.CHAT_ID)
    except Exception:
        return False


def is_reaction_by_admin(update: Update) -> bool:
    try:
        return is_reaction_by_me(update) or (
                update.message_reaction.user and update.message_reaction.user.id in constants.ADMIN_IDS)
    except Exception:
        return False


async def get_message_text_from_id(mid, chat_id, bot) -> str:
    message: Message = await bot.forward_message(chat_id, chat_id, mid, disable_notification=True)
    text = gtext(message)
    await message.delete()
    return text


async def get_message_photo_from_id(mid, chat_id, bot) -> None:
    m = await bot.forward_message(chat_id, chat_id, mid, disable_notification=True)

    try:
        file_id = m.photo[-1].file_id  # Get the file ID of the largest photo size
        file = await bot.get_file(file_id)  # Get the File object from the bot
        await file.download_to_drive('looking_at.jpg')  # Download the file to the local directory
    except Exception as e:
        raise e
    finally:
        await m.delete()
    return


async def get_message_photo_from_message(m, context) -> None:
    try:
        file_id = m.photo[-1].file_id  # Get the file ID of the largest photo size
        file = await context.bot.get_file(file_id)  # Get the File object from the bot
        await file.download_to_drive('looking_at.jpg')  # Download the file to the local directory
    except Exception as e:
        raise e


async def get_message_forward_copy_from_id(mid, chat_id, bot) -> Message:
    message_forward = await bot.forward_message(chat_id, chat_id, mid, disable_notification=True)
    return message_forward


def has_reacted_with(r_type: ReactionType, reaction: MessageReactionUpdated) -> bool:
    return r_type in reaction.new_reaction and r_type not in reaction.old_reaction
