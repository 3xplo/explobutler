from telegram import Update
from telegram.ext import Application, MessageHandler, MessageReactionHandler, filters

import constants
from api_requests.to_telegram import send_scheduled_message
from handlers import private_handler, pin_handler, no_reply_handler, reply_handler, photo_handler, none_handler


def main() -> None:
    """Start the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(constants.ENV_TELEGRAM_BOT_TOKEN).build()
    application.add_handler(MessageHandler(filters.UpdateType.EDITED, none_handler))

    application.add_handler(MessageHandler(filters.StatusUpdate.PINNED_MESSAGE, pin_handler))
    application.add_handler(
        MessageHandler(filters.PHOTO & (filters.REPLY | filters.IS_AUTOMATIC_FORWARD), photo_handler))

    application.add_handler(
        MessageHandler(filters.ChatType.PRIVATE & ~filters.User(constants.EXPLO_ID) & ~filters.User(constants.BOT_ID),
                       private_handler))
    application.add_handler(MessageHandler(filters.TEXT & filters.REPLY & ~filters.User(constants.BOT_ID),
                                           reply_handler))
    application.add_handler(MessageHandler(~filters.REPLY & ~filters.User(constants.EXPLO_ID), no_reply_handler))
    application.add_handler(MessageReactionHandler(reply_handler, user_id=constants.EXPLO_ID,
                                                   message_reaction_types=-1))
    application.add_handler(MessageReactionHandler(reply_handler, message_reaction_types=-1))

    job_queue = application.job_queue

    # Schedule message sending every hour
    job_queue.run_repeating(send_scheduled_message, interval=1800)

    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()
