import enum
import os
from dotenv import load_dotenv

from telegram import ReactionTypeCustomEmoji, ReactionTypeEmoji

load_dotenv()  # take environment variables from .env.
ENV_OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
ENV_TELEGRAM_BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN')

PHOTO_EMOJI = ReactionTypeCustomEmoji('5235837920081887219')
PAINT_EMOJI = ReactionTypeCustomEmoji('5431456208487716895')
PEOPLE_EMOJI = ReactionTypeCustomEmoji('5386539642369614675')
FIREWORKS_EMOJI = ReactionTypeCustomEmoji('5431783411981228752')
TALK_TYPE_EMOJIS = (ReactionTypeCustomEmoji('5372981976804366741'),
                    ReactionTypeCustomEmoji('5458603043203327669'),
                    ReactionTypeEmoji('👍'),
                    ReactionTypeEmoji('😢'),
                    ReactionTypeEmoji('🤔'),
                    ReactionTypeEmoji('😱')
                    )
TALK_EMOJI = ReactionTypeCustomEmoji('5465143921912846619')
LOOKING_GLASS_EMOJI = ReactionTypeCustomEmoji('5231012545799666522')
NEW_EMOJI = ReactionTypeCustomEmoji('5361979468887893611')

EXPLO_ID = 465975653
EXPLO_GROUP_ID = 1087968824
CHAT_ID = -1002056062506
BOT_ID = 7169692630
LOG_CHAT_ID = -1002049063982
ALEX_ID = 309208984
ADMIN_IDS = (EXPLO_ID, EXPLO_GROUP_ID, ALEX_ID)

AUTOANSWER_RATE = 0.2
AUTOANSWER_VOICE_RATE = 0.7


class GeneratedImageType(enum.Enum):
    DALLE_THREE = 3
    DALLE_TWO = 2
    DALLE_TWO_4 = 24
