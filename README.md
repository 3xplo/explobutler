# exploButler Telegram Bot

This project contains the source code for the exploButler Telegram bot, a conversational AI bot designed to interact
with users in a Telegram channel.

## Features

- **Conversational AI**: exploButler uses OpenAI's GPT-4 model to generate human-like responses to user messages and
  questions.
- **Image Generation**: The bot can generate images based on user prompts using DALL-E 2 and DALL-E 3 models.
- **Image Description**: exploButler can analyze images and provide detailed descriptions using GPT-4 Vision.
- **Text-to-Speech**: The bot can convert text responses to speech using OpenAI's text-to-speech model.
- **Scheduled Messages**: exploButler can send scheduled messages to the channel.
- **Reaction-Based Interactions**: Users can interact with the bot by reacting to messages with specific emojis,
  triggering different functionalities.

## Dependencies

- Python 3.8+
- python-telegram-bot
- openai
- python-dotenv

## Installation and Setup

### Clone the repository

```
git clone https://github.com/your-username/exploButler.git 
```

### Install the required dependencies

```
pip install -r requirements.txt
```

### Customize

- Create a .env file in the project directory and add the following environment variables:

```
OPENAI_API_KEY=your_openai_api_key
TELEGRAM_BOT_TOKEN=your_telegram_bot_token
```

- Replace the placeholder values in constants.py with your desired settings, such as chat IDs and admin IDs.

### Run the bot

```
python main.py 
```

### Usage

Once the bot is running, it will listen for messages and reactions in the specified Telegram channel. Users can interact
with the bot by sending messages, replying to messages with specific emojis, or reacting to messages with the designated
emojis.

### License

This project is licensed under the MIT License.