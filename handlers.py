import os
import random

from telegram import Update
from telegram.ext import ContextTypes

import constants
import helpers.message as m_helper
from api_requests.to_openai import generate_text, generate_audio, generate_image_description
from api_requests.to_telegram import send_generated_images
from constants import GeneratedImageType
from helpers.tools import check_bear_string, is_swearing, time_now


async def private_handler(update, _context):
    if update and update.message:
        await update.message.reply_text("https://youtu.be/1Qo35ldlvWo?si=MzU1s0XiUDN5_lvW")


async def pin_handler(update, _context):
    if update and update.message:
        await update.message.delete()


async def no_reply_handler(update: Update, _context: ContextTypes.DEFAULT_TYPE) -> None:
    if update and update.message and update.message.from_user.first_name != "Telegram":
        await update.message.reply_text(
            "Пожалуйста, не шлите в этот чат сообщения напрямую - их не видно на основном канале.\n\nОтвечайте на "
            "другие сообщения, или на сам пост. Спасибо!")


async def reply_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    # Updates by me
    if m_helper.is_reaction_by_admin(update):
        r = update.message_reaction
        if m_helper.has_reacted_with(constants.TALK_EMOJI, r):

            temp_message = await m_helper.get_message_forward_copy_from_id(r.message_id, r.chat.id, context.bot)
            replied_to_text = m_helper.gtext(temp_message)

            ai_text = generate_text(replied_to_text, "")
            if "ERROR:" not in ai_text:
                if random.random() < 0.7:
                    await context.bot.send_message(r.chat.id, ai_text, reply_to_message_id=r.message_id)
                else:
                    await context.bot.send_voice(r.chat.id, generate_audio(ai_text), reply_to_message_id=r.message_id)
                    os.remove("speech.ogg")
            else:
                if update.message:
                    await update.message.set_reaction('😱', True)
                link_message = update.message.reply_to_message.link or 'Ссылка недоступна.' if update.message else ''
                await context.bot.send_message(constants.EXPLO_ID, f"{ai_text} \n\n{link_message}")
            await temp_message.delete()
        if m_helper.has_reacted_with(constants.PHOTO_EMOJI, r):
            text = await m_helper.get_message_text_from_id(r.message_id, r.chat.id, context.bot)
            await send_generated_images(r.chat.id, r.message_id, text, context, GeneratedImageType.DALLE_THREE)
        if m_helper.has_reacted_with(constants.PAINT_EMOJI, r):
            text = await m_helper.get_message_text_from_id(r.message_id, r.chat.id, context.bot)
            await send_generated_images(r.chat.id, r.message_id, text, context, GeneratedImageType.DALLE_TWO)
        if m_helper.has_reacted_with(constants.PEOPLE_EMOJI, r):
            text = await m_helper.get_message_text_from_id(r.message_id, r.chat.id, context.bot)
            await send_generated_images(r.chat.id, r.message_id, text, context, GeneratedImageType.DALLE_TWO_4)
        if m_helper.has_reacted_with(constants.LOOKING_GLASS_EMOJI, r):
            description = ''
            try:
                await m_helper.get_message_photo_from_id(r.message_id, r.chat.id, context.bot)
                description = await generate_image_description(context)
                if 'ERROR:' in description:
                    raise Exception(description)
                ai_text = generate_text(description, "")
                # send reply
                if "ERROR:" not in ai_text:
                    if random.random() < 0.7:
                        await context.bot.send_message(r.chat.id, ai_text, reply_to_message_id=r.message_id)
                    else:
                        await context.bot.send_voice(r.chat.id, generate_audio(ai_text),
                                                     reply_to_message_id=r.message_id)
                        os.remove("speech.ogg")
                else:
                    raise Exception(f'{description} \n\n {ai_text}')

            except Exception as e:
                await context.bot.send_message(constants.EXPLO_ID,
                                               f"Не получилось сгенерировать: {e}\n\n '{description}'")
        if m_helper.has_reacted_with(constants.NEW_EMOJI, r):
            description = ''
            try:
                await m_helper.get_message_photo_from_id(r.message_id, r.chat.id, context.bot)
                description = await generate_image_description(context)
                if 'ERROR:' in description:
                    raise Exception(description)
                await send_generated_images(r.chat.id, r.message_id, description, context,
                                            GeneratedImageType.DALLE_THREE)
            except Exception as e:
                await context.bot.send_message(constants.EXPLO_ID,
                                               f"Не получилось сгенерировать: {e}\n\n '{description}'")

    if m_helper.is_reaction_by_me(update):
        r = update.message_reaction
        if m_helper.has_reacted_with(constants.FIREWORKS_EMOJI, r):
            text = await m_helper.get_message_text_from_id(r.message_id, r.chat.id, context.bot)
            await send_generated_images(r.chat.id, r.message_id, text, context, GeneratedImageType.DALLE_THREE)
            await send_generated_images(r.chat.id, r.message_id, text, context, GeneratedImageType.DALLE_THREE)
            await send_generated_images(r.chat.id, r.message_id, text, context, GeneratedImageType.DALLE_THREE)
            await send_generated_images(r.chat.id, r.message_id, text, context, GeneratedImageType.DALLE_THREE)

    # Updates by others
    if update and update.message_reaction and not m_helper.is_reaction_by_me(update):
        r = update.message_reaction
        for emoji in constants.TALK_TYPE_EMOJIS:
            if m_helper.has_reacted_with(emoji, r):
                temp_message = await m_helper.get_message_forward_copy_from_id(r.message_id, r.chat.id, context.bot)
                replied_to_text = m_helper.gtext(temp_message)

                ai_text = generate_text(replied_to_text, "")
                if "ERROR:" not in ai_text:
                    if random.random() < 0.7:
                        await context.bot.send_message(r.chat.id, ai_text, reply_to_message_id=r.message_id)
                    else:
                        await context.bot.send_voice(r.chat.id, generate_audio(ai_text),
                                                     reply_to_message_id=r.message_id)
                        os.remove("speech.ogg")
                else:
                    if update.message:
                        await update.message.set_reaction('😱', True)
                    link_message = (
                        update.message.reply_to_message.link or 'Ссылка недоступна.' if update.message else '')
                    await context.bot.send_message(constants.EXPLO_ID, f"{ai_text} \n\n{link_message}")
                await temp_message.delete()
                break

    if update and update.message:
        text = m_helper.gtext(update.message)
        replied_to_text = m_helper.gtext(update.message.reply_to_message)
        if "?" in text and replied_to_text and random.random() < constants.AUTOANSWER_RATE:
            if update.message.reply_to_message.from_user.username == "exploButler_bot":
                await update.message.set_reaction('🤷\u200d♂️', True)
            else:
                ai_text = generate_text(replied_to_text, text)
                if "ERROR:" not in ai_text:
                    if random.random() > constants.AUTOANSWER_VOICE_RATE:
                        await update.message.reply_to_message.reply_text(ai_text)
                    else:
                        await update.message.reply_to_message.reply_voice(generate_audio(ai_text))
                else:
                    await update.message.set_reaction('😱', True)
                    link_message = (
                        update.message.reply_to_message.link or 'Ссылка недоступна.' if update.message else '')
                    await context.bot.send_message(constants.EXPLO_ID, f"{ai_text} \n\n{link_message}")
        if check_bear_string(text):
            await update.message.set_reaction('⚡', True)
        elif '❤️' in text:
            await update.message.set_reaction('❤', True)
        elif is_swearing(text):
            await update.message.set_reaction('🙊', True)
        elif 'привет' in text.lower():
            await update.message.set_reaction('🤝', True)


async def photo_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    # print(",\n".join(str(update).split(',')))

    if random.random() > constants.AUTOANSWER_RATE:
        return

    description = ''
    try:
        r = update.message
        await m_helper.get_message_photo_from_message(r, context)
        description = await generate_image_description(context)
        if 'ERROR:' in description:
            raise Exception(description)
        ai_text = generate_text(description, "")
        # send reply
        if "ERROR:" not in ai_text:
            if random.random() < 0.7:
                await context.bot.send_message(r.chat.id, ai_text, reply_to_message_id=r.message_id)
            else:
                await context.bot.send_voice(r.chat.id, generate_audio(ai_text), reply_to_message_id=r.message_id)
                os.remove("speech.ogg")
        else:
            raise Exception(f'{description} \n\n {ai_text}')

    except Exception as e:
        await context.bot.send_message(constants.EXPLO_ID, f"Не получилось сгенерировать: {e}\n\n '{description}'")


async def channel_post_handler(_update: Update, _context: ContextTypes.DEFAULT_TYPE) -> None:
    print("channel_post_handler: ", time_now())
    pass


async def none_handler(_update, _context):
    pass
