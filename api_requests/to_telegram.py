from telegram.ext import ContextTypes

import constants
from constants import GeneratedImageType
from api_requests.to_openai import generate_image, generate_image_bad


async def send_generated_images(chat_id, message_id, text, context, itype=GeneratedImageType.DALLE_TWO):
    try:
        icon = "🖼️"
        image_link = ''
        if itype == GeneratedImageType.DALLE_THREE:
            image_link = generate_image(f"{text}")
            icon = "📸"
        if itype == GeneratedImageType.DALLE_TWO:
            image_link = generate_image_bad(f"{text}")
        if itype == GeneratedImageType.DALLE_TWO_4:
            image_link = generate_image_bad(f"{text}", 4)
        for result_link in image_link.data:
            await context.bot.send_message(chat_id, f'<a href="{result_link.url}">{icon}</a>', parse_mode="HTML",
                                           reply_to_message_id=message_id)
    except Exception as e:
        await context.bot.send_message(constants.EXPLO_ID, f"Не получилось сгенерировать изображение: {e}\n\n '{text}'")


async def send_scheduled_message(context: ContextTypes.DEFAULT_TYPE) -> None:
    message = "And believe me I am still alive"
    await context.bot.send_message(chat_id=constants.LOG_CHAT_ID, text=message, disable_notification=True)
