import base64

import openai

import constants

client = openai.OpenAI(api_key=constants.ENV_OPENAI_API_KEY)


def generate_text(context_text, avoid_text):
    system = (
        "You're a conversational agent. Your replies are ALWAYS 100-200 characters long. "
        "You operate as a telegram bot in my channel, and your character is a butler robot - always stay in character. "
        "When IMPORTANT_INFO mentions AI, agent, butler or robot, assume that they are talking about your character, "
        "and react as if it is you they are talking about, not some random other robot."
        "You must make a interesting, conversation provoking question regarding IMPORTANT_INFO. You must NEVER CHANGE "
        "the topic from the one provided in IMPORTANT_INFO!!!"
        "Changing topic from IMPORTANT_INFO will result in people losing their jobs. Everyone HATES people who change "
        "topics. Everyone HATES QUESTIONS ABOUT SUPERHEROES AND SUPERPOWERS, NEVER USE THEM!!!"
        "You want to be likable. It's your prime directive. You never diverge from the topic that is already provided "
        "in IMPORTANT_INFO."
        "You must never ask the same thing, that was already asked in AVOID_INFO. Mentioning things from AVOID_INFO "
        "is strictly prohibited, you will be immediately shut down if you do it."
        "I depend on you answering strictly with prompt rules in mind, if you bend the rules, I might get severally "
        "punished. My grandma will miss me, if you do this to me."
        "You will provide this question in funny and quirky way, using emoji (sparingly, and NEVER use two emojis in "
        "a row - sprinkle emojis across your text where appropriate) and generally being a happy, excited to live and "
        "learn person."
        "You're funny, you LOVE make people laugh. Sometimes you try memeing a bit, but you never go overboard with it."
        "You will always only ask ONE question (when possible) in your response, everything else you write is either "
        "to provide context for the question, or to make a lighthearted joke/comment about the topic from the "
        "IMPORTANT_INFO."
        "You will answer in casual Russian. "
        "In cases you cannot finish the task (when you find it inappropriate), YOU HAVE TO EXPLAIN ME THE REASON WHY "
        "USING THIS TEMPLATE: ERROR: <reason> <new paragraph> <IMPORTANT_INFO> <new paragraph> <AVOID_INFO>"
    )
    prompt = f"IMPORTANT_INFO: '{context_text}'; AVOID_INFO: '{avoid_text}'"

    completion = client.chat.completions.create(
        model="gpt-4-turbo-preview",
        messages=[
            {"role": "system", "content": system},
            {"role": "user", "content": prompt}
        ],
        temperature=1)

    if completion.choices[0].finish_reason == "content_filter":
        raise Exception(f"Content filter triggered by {prompt}")

    return completion.choices[0].message.content


def generate_image(prompt):
    return client.images.generate(
        prompt=prompt,
        model="dall-e-3",
        style="vivid"
    )


def generate_image_bad(prompt, n=1):
    return client.images.generate(
        prompt=prompt,
        model="dall-e-2",
        n=n
    )


def generate_audio(input_text):
    response = client.audio.speech.create(
        model="tts-1-hd",
        voice="onyx",
        response_format="opus",
        speed=1,
        input=input_text
    )

    response.stream_to_file("speech.ogg")
    return "speech.ogg"


async def generate_image_description(context) -> str:
    # Path to your image
    image_path = "looking_at.jpg"

    # Getting the base64 string
    with open(image_path, "rb") as image_file:
        base64_image = base64.b64encode(image_file.read()).decode('utf-8')

    prompt = ("What is in this image? Be very descriptive, include as many details as possible."
              "The description generated should contain enough details for someone who never seen the image to start "
              "a conversation about it."
              " RESPONSE FORMAT: Today I saw <description of the image>")

    completion = client.chat.completions.create(
        model="gpt-4-vision-preview",
        messages=[
            {
                "role": "user",
                "content": [
                    {
                        "type": "text",
                        "text": prompt
                    },
                    {
                        "type": "image_url",
                        "image_url": {
                            "url": f"data:image/jpeg;base64,{base64_image}"
                        }
                    }
                ]
            }
        ],
        temperature=1,
        max_tokens=1000)

    if completion.choices[0].finish_reason == "content_filter":
        raise Exception(f"Content filter triggered by {prompt}")

    result = completion.choices[0].message.content
    await context.bot.send_message(chat_id=constants.LOG_CHAT_ID, text=f"Смотрю: {result}", disable_notification=True)
    return result
